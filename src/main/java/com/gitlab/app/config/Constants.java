package com.gitlab.app.config;

public class Constants {
    static final String CSV_TEST_DATA_FILE_PATH = "src/test/resources/csv/";
    static final String EXCEL_TEST_DATA_FILE_PATH = "src/test/resources/excel/";
    static final String JSON_TEST_DATA_FILE_PATH = "src/test/resources/json/";
    static final String XML_TEST_DATA_FILE_PATH = "src/test/resources/xml/";
}
