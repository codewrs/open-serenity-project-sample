/*
 *  Copyright (c) 2019.  gitlab.com
 */

package com.gitlab.app.config;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Open Serenity Automation Project
 *
 * <p>This class should provide environment related data
 *
 * @author devwrs
 * @version 1.0 7/11/2019
 */
public class DataProvider {
    private static final Logger LOG = LoggerFactory.getLogger(DataProvider.class);

    private static final EnvironmentVariables VARIABLES =
            SystemEnvironmentVariables.createEnvironmentVariables();
    public static final String DEFAULT_ENVIRONMENT =
            VARIABLES.getProperty("open.app.test.env.default").toLowerCase();

    public static final String CSV_TEST_DATA_FILE_PATH = Constants.CSV_TEST_DATA_FILE_PATH;
    public static final String EXCEL_TEST_DATA_FILE_PATH = Constants.EXCEL_TEST_DATA_FILE_PATH;
    public static final String JSON_TEST_DATA_FILE_PATH = Constants.JSON_TEST_DATA_FILE_PATH;
    public static final String XML_TEST_DATA_FILE_PATH = Constants.XML_TEST_DATA_FILE_PATH;

    private String testEnvironment;

    public DataProvider() {
        initProperties();
    }

    public String getTestEnvironment() {
        return testEnvironment;
    }

    private void setTestEnvironment(String testEnvironment) {
        this.testEnvironment =
                testEnvironment == null ? DEFAULT_ENVIRONMENT : testEnvironment.toLowerCase();
    }

    public String getSerenityProperty(String key) throws Exception {
        key = key.contains("$ENV") ? addEnvironment(key, testEnvironment) : key;
        String value = VARIABLES.getProperty(key);

        if (value != null)
            return value.contains("$ENV") ? addEnvironment(value, testEnvironment) : value;
        else throw new Exception("Property Not Found [" + key + "]");
    }

    private void initProperties() {
        // SET Test Environment
        setTestEnvironment(VARIABLES.getProperty("tenv"));
    }

    private String addEnvironment(String string, String env) {
        return string == null ? null : string.replace("$ENV", env);
    }
}
