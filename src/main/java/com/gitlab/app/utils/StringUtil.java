package com.gitlab.app.utils;

import java.util.Map;

/**
 * Open Serenity Automation Project
 *
 * <p>String Util
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/29/2019
 */
public class StringUtil {

    /**
     * Prepare test data string. Replace all the keys in the Map with the value.
     *
     * @param attributes Map that contains key to replace with value
     * @param testData String test data
     * @return String after replacing the strings
     */
    public static String prepareTestDataString(Map<String, String> attributes, String testData) {
        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            testData = testData.replaceAll(entry.getKey(), entry.getValue());
        }

        return testData;
    }

    /**
     * Check if a string value is Numeric
     *
     * @param string String
     * @return boolean
     */
    public static boolean isNumeric(String string) {

        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException ignored) {
        }

        try {
            Double.parseDouble(string);
            return true;
        } catch (NumberFormatException ignored) {
        }

        try {
            Long.parseLong(string);
            return true;
        } catch (NumberFormatException ignored) {
        }

        return false;
    }

    /**
     * Check if a string value is Integer
     *
     * @param string String
     * @return boolean
     */
    public static boolean isInteger(String string) {

        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException ignored) {
            return false;
        }
    }

    /**
     * Check if a String value is Double
     *
     * @param string String
     * @return boolean
     */
    public static boolean isDouble(String string) {

        try {
            Double.valueOf(string);
            return true;
        } catch (NumberFormatException ignored) {
            return false;
        }
    }
}
