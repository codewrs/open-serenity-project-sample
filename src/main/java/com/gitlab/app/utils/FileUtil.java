package com.gitlab.app.utils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;

/**
 * Open Serenity Automation Project
 *
 * <p>File UTil Class
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/25/2019
 */
public class FileUtil {
    public FileUtil() {}

    /**
     * Read File
     *
     * @param fileName Full path of file
     * @return String
     * @throws IOException If file not found
     */
    public String readFile(String fileName) throws IOException {
        File file = new File(fileName);

        if (!file.isFile()) throw new FileNotFoundException(fileName);

        return readFileUsingIOUtils(file);
    }

    /**
     * Create File Object
     *
     * @param fileName Full path of file
     * @return File Object
     * @throws IOException If file not found
     */
    public File getFileObject(String fileName) throws IOException {
        File file = new File(fileName);

        if (!file.isFile()) throw new FileNotFoundException(fileName);

        return file;
    }

    /**
     * Read File
     *
     * @param fileName Full path of file
     * @param splitCharacter Split character
     * @return Array of Strings
     * @throws IOException If file not found
     */
    public String[] readFile(String fileName, String splitCharacter) throws IOException {
        File file = new File(fileName);

        if (!file.isFile()) throw new FileNotFoundException(fileName);

        return readFileUsingIOUtils(file).split(splitCharacter);
    }

    private String readFileUsingIOUtils(File file) throws IOException {
        String fileContent;

        try (InputStream inputStream = new FileInputStream(file)) {
            fileContent = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        }

        return fileContent;
    }
}
