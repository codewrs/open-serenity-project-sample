package com.gitlab.app.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Open Serenity Automation Project
 *
 * <p>Json util
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/3/2019 8:09 PM
 */
public class JsonUtils {

    /**
     * Read json file
     *
     * @param fileName Json file name
     * @return String
     * @throws FileNotFoundException if file not found
     */
    public String getJSONStringFromFile(String fileName) throws FileNotFoundException {

        return readJSONFromFile(fileName).toString();
    }

    /**
     * Read json file
     *
     * @param fileName Json file name
     * @return Json Object
     * @throws FileNotFoundException if file not found
     */
    public JSONObject getJSONFromFile(String fileName) throws FileNotFoundException {

        return (JSONObject) readJSONFromFile(fileName);
    }

    /**
     * Read json file
     *
     * @param fileName Json file name
     * @return Json Object
     * @throws FileNotFoundException if file not found
     */
    public JSONArray getJSONArrayFromFile(String fileName) throws FileNotFoundException {

        return (JSONArray) readJSONFromFile(fileName);
    }

    /**
     * Convert json string to Json Object
     *
     * @param json String
     * @return Json Object
     */
    public JSONObject getJSONFromString(String json) {
        JSONTokener jsonTokener = new JSONTokener(json);

        return new JSONObject(jsonTokener);
    }

    private Object readJSONFromFile(String fileName) throws FileNotFoundException {
        File inputJSONFile = new File(fileName);

        if (!inputJSONFile.isFile()) throw new FileNotFoundException(fileName);

        ObjectMapper mapper = new ObjectMapper();
        Object object = null;
        try {
            JsonNode node = mapper.readTree(inputJSONFile);
            String nodeString = node.toString();

            if (node.isArray()) object = new JSONArray(nodeString);
            else object = new JSONObject(nodeString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return object;
    }
}
