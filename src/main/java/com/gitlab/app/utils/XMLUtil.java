package com.gitlab.app.utils;

import java.io.File;
import java.io.FileNotFoundException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.XMLUnit;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Open Serenity Automation Project
 *
 * <p>XML Util
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/29/2019
 */
public class XMLUtil {

    private Document xmlObject;

    /**
     * Check if two XML file are equal
     *
     * @param expected Expected XML in String format
     * @param actual Actual XML in String format
     * @return boolean
     * @throws Exception in case of error
     */
    public static boolean isXMLEquals(String expected, String actual) throws Exception {
        XMLUnit.setIgnoreWhitespace(true);

        DetailedDiff diff = new DetailedDiff(XMLUnit.compareXML(expected, actual));

        return diff.identical();
    }

    /**
     * Create XML Object
     *
     * @param fileName XML file Name
     * @return Document object
     * @throws Exception in case of error
     */
    public XMLUtil getXMLObject(String fileName) throws Exception {
        File file = new File(fileName);

        if (!file.isFile()) throw new FileNotFoundException(fileName);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        this.xmlObject = factory.newDocumentBuilder().parse(file);

        return this;
    }

    /**
     * Get XML node value
     *
     * @param expression Xpath expression
     * @return String node value
     * @throws XPathExpressionException if the expression is invalid
     */
    public String getNodeValue(String expression) throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        Node node = (Node) xPath.compile(expression).evaluate(xmlObject, XPathConstants.NODE);

        return node.getTextContent();
    }

    /**
     * Get XML node value
     *
     * @param expression Xpath expression
     * @param attributeName Name of the attribute
     * @return String attribute value
     * @throws XPathExpressionException if the expression is invalid
     */
    public String getNodeAttributeValue(String expression, String attributeName)
            throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        Node node = (Node) xPath.compile(expression).evaluate(xmlObject, XPathConstants.NODE);

        return node.getAttributes().getNamedItem(attributeName).getNodeValue();
    }

    /**
     * Get XML node list
     *
     * @param expression Xpath expression
     * @return NodeList node value
     * @throws XPathExpressionException if the expression is invalid
     */
    public NodeList getNodeList(String expression) throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();

        return (NodeList) xPath.compile(expression).evaluate(xmlObject, XPathConstants.NODESET);
    }
}
