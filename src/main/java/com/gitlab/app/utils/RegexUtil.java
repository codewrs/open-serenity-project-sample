package com.gitlab.app.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Open Serenity Automation Project
 *
 * <p>Regex Util
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/3/2019 8:10 PM
 */
public class RegexUtil {

    /**
     * Match a regex pattern and return the value
     *
     * @param regex pattern to match
     * @param text String to search On
     * @return Integer if match found else null
     */
    public static String getRegexStringValue(String regex, String text) {
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(text);

        int index = 0;
        if (regex.contains("(") && regex.contains(")")) {
            index = 1;
        }

        while (matcher.find()) {
            return matcher.group(index);
        }
        return null;
    }

    /**
     * Match a regex pattern and return the value
     *
     * @param regex pattern to match
     * @param text String to search On
     * @return Integer if match found else null
     */
    public static int getRegexIntegerValue(String regex, String text) {
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(text);

        int index = 0;
        if (regex.contains("(") && regex.contains(")")) {
            index = 1;
        }

        while (matcher.find()) {
            return Integer.parseInt(matcher.group(index));
        }
        return index;
    }
}
