package com.gitlab.app.utils;

import java.sql.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Open Serenity Automation Project
 *
 * <p>Database Connection Util
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/3/2019 7:33 PM
 */
public class DBUtil {
    private static DBUtil dbUtil;
    private static Connection connection;
    private static String host;
    private static String username;
    private static String password;
    private static final Logger LOG = LoggerFactory.getLogger(DBUtil.class);

    private DBUtil() {}

    public static DBUtil getInstance() {

        if (dbUtil == null) dbUtil = new DBUtil();

        return dbUtil;
    }

    public void setHost(String host) {
        DBUtil.host = host;
    }

    public void setUsername(String username) {
        DBUtil.username = username;
    }

    public void setPassword(String password) {
        DBUtil.password = password;
    }

    public synchronized Connection getJDBCToAWSRDSConnection(int waitTimeInSeconds)
            throws Exception {

        if (connection == null || connection.isClosed()) {

            while (waitTimeInSeconds > 0) {
                try {
                    connection = DriverManager.getConnection(host, username, password);
                    break;
                } catch (Exception e) {
                    LOG.info("Waiting for connection...");
                }
                waitTimeInSeconds--;
                Thread.sleep(1000);
            }

            if (connection == null || connection.isClosed())
                throw new SQLException("Failed to connect to Database!");
        }

        return connection;
    }

    /**
     * Query Database
     *
     * @param connection Connection Instance
     * @param query Select query
     * @return String
     * @throws Exception If the query is wrong
     */
    public ResultSet queryDatabase(Connection connection, String query) throws Exception {

        return connection.prepareStatement(FilterUtil.filterSelectQuery(query)).executeQuery();
    }

    /**
     * Delete from Database
     *
     * @param connection Connection Instance
     * @param query Delete Query
     * @return Integer value of the affected rows
     * @throws Exception If the query is wrong
     */
    public int deleteFromDatabase(Connection connection, String query) throws Exception {

        return connection.prepareStatement(FilterUtil.filterDeleteQuery(query)).executeUpdate();
    }

    /**
     * Insert Into Database
     *
     * @param connection Connection Instance
     * @param query Insert Query
     * @return Integer value of the affected rows
     * @throws Exception If the query is wrong
     */
    public int insertIntoDatabase(Connection connection, String query) throws Exception {
        Statement statement = connection.createStatement();
        String[] queryArray = FilterUtil.filterInsertQuery(query);

        connection.setAutoCommit(false);
        for (String q : queryArray) {
            statement.addBatch(q);
        }

        int[] rowsAffected = statement.executeBatch();
        connection.commit();

        return rowsAffected.length;
    }

    /**
     * Update Database
     *
     * @param connection Connection Instance
     * @param query Update Query
     * @return Integer value of the affected rows
     * @throws Exception If the query is wrong
     */
    public int updateDatabase(Connection connection, String query) throws Exception {
        Statement statement = connection.createStatement();
        String[] queryArray = FilterUtil.filterUpdateQuery(query);

        connection.setAutoCommit(false);
        for (String q : queryArray) {
            statement.addBatch(q);
        }

        int[] rowsAffected = statement.executeBatch();
        connection.commit();

        return rowsAffected.length;
    }

    /**
     * Close connection
     *
     * @throws SQLException If connection cannot be closed
     */
    public void closeConnection() throws SQLException {

        if (connection != null) {
            connection.close();
        }
    }
}
