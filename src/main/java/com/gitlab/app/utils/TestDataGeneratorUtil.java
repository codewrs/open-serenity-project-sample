package com.gitlab.app.utils;

import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Open Serenity Automation Project
 *
 * <p>Test Data Generator Class.
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/25/2019
 */
public class TestDataGeneratorUtil {

    protected static final Logger LOG = LoggerFactory.getLogger(TestDataGeneratorUtil.class);

    /**
     * Generate File List ID
     *
     * @return String of Alphanumeric UUID (32 chars)
     */
    public static String generateUUID() throws Exception {

        return UUID.randomUUID().toString();
    }
}
