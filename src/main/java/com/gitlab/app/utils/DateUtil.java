package com.gitlab.app.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Open Serenity Automation Project
 *
 * <p>Date Util
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/25/2019
 */
public class DateUtil {

    /**
     * Get current system date or timestamp
     *
     * @param dateFormat String Date format eg. "MM/dd/yyyy" or ddMMyyyyHHmmss
     * @return String
     */
    public static String getDate(String dateFormat) {
        String datetime = null;

        if (dateFormat == null || dateFormat.isEmpty()) {
            dateFormat = "MM/dd/yyyy";
        }
        // eg. pattern "yyyy/MM/dd HH:mm:ss"
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
        String formatchars = dateFormat.toLowerCase();
        if (formatchars.contains("h") || formatchars.contains("m") || formatchars.contains("s")) {
            LocalDateTime ldate = LocalDateTime.now();
            datetime = ldate.format(dtf);
        } else {
            LocalDate ldate = LocalDate.now();
            datetime = ldate.format(dtf);
        }

        return datetime;
    }

    /**
     * Get current system date or timestamp
     *
     * @param dateFormat String Date format eg. "MM/dd/yyyy" or ddMMyyyyHHmmss
     * @param zoneId String zond ID eg. UTC
     * @return String
     */
    public static String getDate(String dateFormat, String zoneId) {
        String datetime = null;

        if (dateFormat == null || dateFormat.isEmpty()) {
            dateFormat = "MM/dd/yyyy";
        }
        // eg. pattern "yyyy/MM/dd HH:mm:ss"
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
        String formatchars = dateFormat.toLowerCase();
        if (formatchars.contains("h") || formatchars.contains("m") || formatchars.contains("s")) {
            LocalDateTime ldate = LocalDateTime.now(ZoneId.of(zoneId));
            datetime = ldate.format(dtf);
        } else {
            LocalDate ldate = LocalDate.now();
            datetime = ldate.format(dtf);
        }

        return datetime;
    }

    /**
     * Get current system date or timestamp
     *
     * @param dateFormat String Date format eg. "MM/dd/yyyy" or ddMMyyyyHHmmss
     * @param zoneId String zond ID eg. UTC
     * @return long
     */
    public static long getDateInMilliSeconds(String dateFormat, String zoneId) {
        long datetime = 0;

        if (dateFormat == null || dateFormat.isEmpty()) {
            dateFormat = "MM/dd/yyyy";
        }
        // eg. pattern "yyyy/MM/dd HH:mm:ss"
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
        String formatchars = dateFormat.toLowerCase();
        if (formatchars.contains("h") || formatchars.contains("m") || formatchars.contains("s")) {
            LocalDateTime ldate = LocalDateTime.now(ZoneId.of(zoneId));
            datetime = ldate.atZone(ZoneId.of(zoneId)).toInstant().toEpochMilli();
        } else throw new DateTimeException("Invalid Date Time format [" + dateFormat + "]");

        return datetime;
    }

    /**
     * Convert Date into Milli Seconds
     *
     * @param dateTime Input Date in String
     * @param dateTimeFormat Date format eg. "MM/dd/yyyy hh:mm:ss a"
     * @param zoneId Zone ID in String eg. UTC
     * @return String
     */
    public static long convertDateToMilliSeconds(
            String dateTime, String dateTimeFormat, String zoneId) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateTimeFormat);
        LocalDateTime localDateTime = LocalDateTime.parse(dateTime, dtf);

        return localDateTime.atZone(ZoneId.of(zoneId)).toInstant().toEpochMilli();
    }

    /**
     * Add days to a date
     *
     * @param date Input date in string format
     * @param days Days to add
     * @param dateFormat Optional string Date format eg. "MM/dd/yyyy" or null s * @return String
     */
    public static String plusDays(String date, long days, String dateFormat) {

        if (dateFormat == null || dateFormat.isEmpty()) {
            dateFormat = "MM/dd/yyyy";
        }
        // eg. pattern "yyyy/MM/dd HH:mm:ss"
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
        LocalDate ldate = LocalDate.parse(date, dtf).plusDays(days);

        return ldate.format(dtf);
    }

    /**
     * Minus days from a date
     *
     * @param date Input date in string format
     * @param days Days to subtract
     * @param dateFormat Optional string Date format eg. "MM/dd/yyyy" or null
     * @return String
     */
    public static String minusDays(String date, long days, String dateFormat) {

        if (dateFormat == null || dateFormat.isEmpty()) {
            dateFormat = "MM/dd/yyyy";
        }
        // eg. pattern "yyyy/MM/dd HH:mm:ss"
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
        LocalDate ldate = LocalDate.parse(date, dtf).minusDays(days);

        return ldate.format(dtf);
    }

    /**
     * Check if firstDate is before date from secondDate
     *
     * @param firstDate First input date in string format
     * @param secondDate Second input date in string format
     * @param dateFormat Optional string Date format eg. "MM/dd/yyyy" or null
     * @return boolean
     */
    public static boolean isBeforeDate(String firstDate, String secondDate, String dateFormat) {

        if (dateFormat == null || dateFormat.isEmpty()) {
            dateFormat = "MM/dd/yyyy";
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
        LocalDate firstdate = LocalDate.parse(firstDate, dtf);
        LocalDate seconddate = LocalDate.parse(secondDate, dtf);

        return firstdate.isBefore(seconddate);
    }

    /**
     * Check if firstDate is after date from secondDate
     *
     * @param firstDate First input date in string format
     * @param secondDate Second input date in string format
     * @param dateFormat Optional string Date format eg. "MM/dd/yyyy" or null
     * @return boolean
     */
    public static boolean isAfterDate(String firstDate, String secondDate, String dateFormat) {

        if (dateFormat == null || dateFormat.isEmpty()) {
            dateFormat = "MM/dd/yyyy";
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(dateFormat);
        LocalDate firstdate = LocalDate.parse(firstDate, dtf);
        LocalDate seconddate = LocalDate.parse(secondDate, dtf);

        return firstdate.isAfter(seconddate);
    }

    /**
     * Convert Date format
     *
     * @param date Input date in string format
     * @param fromFormat From format
     * @param toFormat To format
     * @return String
     */
    public static String changeDateFormat(String date, String fromFormat, String toFormat)
            throws ParseException {
        DateFormat dtfFrom = new SimpleDateFormat(fromFormat);
        DateFormat dtfTo = new SimpleDateFormat(toFormat);
        Date dateObject = dtfFrom.parse(date);

        return dtfTo.format(dateObject);
    }
}
