package com.gitlab.app.utils;

/**
 * Open Serenity Automation Project
 *
 * <p>Filter util
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/3/2019 8:09 PM
 */
public class FilterUtil {

    /**
     * Filter Select Query String. Checks if query starts with SELECT keyword and contains LIMIT if
     * not, it adds "LIMIT 1" to the query to limit the result to just one Row
     *
     * @param query Select Query
     * @return Filtered query
     * @throws Exception If the query not select statement
     */
    public static String filterSelectQuery(String query) throws Exception {
        String filtered;
        query = query.replace("\r", "");
        query = query.replace("\n", "");
        query = query.replace("\t", "");
        query = query.toUpperCase();

        if (query.startsWith("SELECT ")) {

            if (!query.contains("LIMIT") && !query.contains("WHERE")) query = query + " LIMIT 1";
            filtered = query;
        } else {
            throw new Exception("Invalid SELECT Query");
        }

        return filtered;
    }

    /**
     * Checks if query starts with INSERT keyword
     *
     * @param query Insert Query
     * @return Filtered query
     * @throws Exception If the query not select statement
     */
    public static String[] filterInsertQuery(String query) throws Exception {
        boolean checked = true;
        query = query.replace("\r", "");
        query = query.replace("\n", "");
        query = query.replace("\t", "");
        String[] queryArray = query.split(";");

        for (String q : queryArray) {
            if (!(q.toUpperCase().startsWith("INSERT "))) {
                checked = false;
                break;
            }
        }
        if (!checked) throw new Exception("Invalid INSERT Query");

        return queryArray;
    }

    /**
     * Checks if query starts with UPDATE keyword and contains WHERE clause
     *
     * @param query Select Query
     * @return Filtered Query
     * @throws Exception If the query not select statement
     */
    public static String[] filterUpdateQuery(String query) throws Exception {
        boolean checked = true;
        query = query.replace("\r", "");
        query = query.replace("\n", "");
        query = query.replace("\t", "");
        String[] queryArray = query.split(";");

        for (String q : queryArray) {
            if (!(q.toUpperCase().startsWith("UPDATE ") && q.toUpperCase().contains("WHERE"))) {
                checked = false;
                break;
            }
        }
        if (!checked) throw new Exception("Invalid UPDATE Query");

        return queryArray;
    }

    /**
     * Checks if query starts with DELETE keyword and contains WHERE clause
     *
     * @param query Select Query
     * @return Filtered String
     * @throws Exception If the query not delete statement and does not contain WHERE clause
     */
    public static String filterDeleteQuery(String query) throws Exception {
        boolean checked = true;
        query = query.replace("\r", "");
        query = query.replace("\n", "");
        query = query.replace("\t", "");
        query = query.toUpperCase();
        String[] queryArray = query.split(";");

        for (String q : queryArray) {
            if (!(q.startsWith("DELETE ") && q.contains("WHERE"))) {
                checked = false;
                break;
            }
        }
        if (!checked) throw new Exception("Invalid DELETE Query");

        return query;
    }
}
