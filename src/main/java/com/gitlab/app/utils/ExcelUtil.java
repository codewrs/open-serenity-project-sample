package com.gitlab.app.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * Open Serenity Automation Project
 *
 * <p>Excel Util
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 8/14/2019
 */
public class ExcelUtil {
    private static ExcelUtil excelUtil;

    private ExcelUtil() {}

    public static synchronized ExcelUtil getInstance() {
        if (excelUtil == null) {
            excelUtil = new ExcelUtil();
        }

        return excelUtil;
    }

    /**
     * Initialize All Sheets
     *
     * @param fileName file name of the excel file
     * @return Map containing sheet name as key and all the cell values of the sheet as String array
     * @throws IOException in case of error
     */
    public Map<String, String[][]> getExcelObject(String fileName) throws IOException {
        File file = new File(fileName);

        if (!file.isFile()) throw new FileNotFoundException(fileName);

        Workbook workbook = WorkbookFactory.create(file);
        Map<String, String[][]> workbookMap = new HashMap<>();
        Iterator<Sheet> iterator = workbook.sheetIterator();

        while (iterator.hasNext()) {
            Sheet sheet = iterator.next();
            workbookMap.put(sheet.getSheetName(), getCellValues(sheet));
        }
        workbook.close();

        return workbookMap;
    }

    /**
     * Initialize Single Sheet
     *
     * @param fileName file name of the excel file
     * @param sheetName name of the sheet
     * @return String[][] containing all the cell values of the sheet
     * @throws IOException in case of error
     */
    public String[][] getExcelObject(String fileName, String sheetName) throws IOException {
        File file = new File(fileName);

        if (!file.isFile()) throw new FileNotFoundException(fileName);

        Workbook workbook = WorkbookFactory.create(file);
        String[][] cellValues = new String[0][];
        Iterator<Sheet> iterator = workbook.sheetIterator();

        while (iterator.hasNext()) {
            Sheet sheet = iterator.next();

            if (sheet.getSheetName().equalsIgnoreCase(sheetName)) {
                cellValues = getCellValues(sheet);
                break;
            }
        }
        workbook.close();

        return cellValues;
    }

    private String[][] getCellValues(Sheet sheet) {
        int maxRow = sheet.getLastRowNum() + 1;
        int maxCol = getMaxColumnNumber(sheet);
        String[][] cellValues = new String[maxRow + 1][maxCol + 1];

        for (int i = 0; i < maxRow; i++) {
            Row row = sheet.getRow(i);
            int lastColumn = getLastCellNumber(row);

            for (int j = 0; j < lastColumn; j++) {
                cellValues[i + 1][j + 1] =
                        String.valueOf(row.getCell(j, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK));
            }
        }

        return cellValues;
    }

    private int getLastCellNumber(Row row) {
        int cellNumber = 0;
        try {
            cellNumber = row.getLastCellNum();
        } catch (NullPointerException ignored) {
        }

        return cellNumber;
    }

    private int getMaxColumnNumber(Sheet sheet) {
        Iterator<Row> rowIterator = sheet.rowIterator();
        int maxColumn = 0;

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            int lastColumn = row.getLastCellNum();

            if (lastColumn > maxColumn) {
                maxColumn = lastColumn;
            }
        }

        return maxColumn;
    }
}
