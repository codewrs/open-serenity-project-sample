package com.gitlab.app.model;

public enum TestEnvironment {
    DEV,
    QA,
    PROD,
    UAT
}
