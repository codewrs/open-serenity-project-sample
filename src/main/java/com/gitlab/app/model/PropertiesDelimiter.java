package com.gitlab.app.model;

public enum PropertiesDelimiter {
    COMMA {
        @Override
        public String toString() {
            return ",";
        }
    }
}
