/*
 * *
 *    Copyright (c) 2019.  gitlab.com
 *
 */

package com.gitlab.app.testbase;

import static io.restassured.RestAssured.baseURI;

import com.gitlab.app.config.DataProvider;
import com.gitlab.app.utils.DBUtil;
import com.gitlab.app.utils.JsonUtils;
import io.restassured.RestAssured;
import java.sql.SQLException;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.steps.StepEventBus;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(SerenityParameterizedRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public abstract class BaseTest {

    protected static final Logger LOG = LoggerFactory.getLogger(BaseTest.class);
    protected static DataProvider provider = new DataProvider();
    protected static int WAIT_TIME;
    protected static DBUtil dbUtil = DBUtil.getInstance();
    protected JsonUtils jsonUtils = new JsonUtils();

    // Test Setup and tear down should be defined here
    @Before
    public void setup() throws Exception {
        WAIT_TIME =
                Integer.parseInt(provider.getSerenityProperty("open.app.test.wait.time.seconds"));
        RestAssured.baseURI =
                provider.getSerenityProperty("open.app.test.service.api.url.base.$ENV");
        RestAssured.port =
                Integer.parseInt(
                        provider.getSerenityProperty("open.app.test.service.api.port.default"));
        RestAssured.useRelaxedHTTPSValidation();
        LOG.info("[Done] Setup" + " Base URI is " + baseURI);
    }

    // Declare as Abstract method if its mandatory cleanup
    @After
    public void tearDown() {
        try {
            dbUtil.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        StepEventBus.getEventBus().clearStepFailures();
        LOG.info("[Done] End Test");
    }
}
