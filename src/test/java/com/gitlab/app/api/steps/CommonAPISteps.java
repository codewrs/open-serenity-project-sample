/*
 *  Copyright (c) 2019.  gitlab.com
 */

package com.gitlab.app.api.steps;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.junit.Assert.assertEquals;

import com.github.fge.jsonschema.SchemaVersion;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.gitlab.app.api.pages.CommonApiObjects;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.thucydides.core.annotations.Step;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Open Serenity Automation Project
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/1/2019 1:09 PM
 */
public class CommonAPISteps {
    protected Response response;
    protected CommonApiObjects apiObjects = new CommonApiObjects();
    private JsonSchemaFactory jsonSchemaFactory =
            JsonSchemaFactory.newBuilder()
                    .setValidationConfiguration(
                            ValidationConfiguration.newBuilder()
                                    .setDefaultVersion(SchemaVersion.DRAFTV4)
                                    .freeze())
                    .freeze();
    private String uri;
    private int statusCode;

    @Step("Given that we have a URI [{0}]")
    public void givenThatWeHaveAURI(String uri) {

        this.uri = uri;
    }

    @Step("And we have a URI [{0}]")
    public void andWeHaveAURI(String uri) {

        this.uri = uri;
    }

    @Step("When a GET API call is made")
    public void whenAGETApiCallIsMade() {

        this.apiObjects.makeGetAPICall(uri);
    }

    @Step("When a POST API call is made")
    public void whenAPOSTApiCallIsMade(String object) {

        this.apiObjects.makePostAPICall(uri, object);
    }

    @Step("When a POST API call is made")
    public void whenAPOSTApiCallIsMade(Map<String, String> paramMap) {

        this.apiObjects.makePostAPICall(uri, paramMap);
    }

    @Step("Then it should return a response with Status Code {0}")
    public Response thenItShouldReturnAResponseWithStatusCode(String statusCode) {

        this.statusCode = Integer.parseInt(statusCode);

        return this.response = apiObjects.getResponse(this.statusCode);
    }

    @Step("And response should have a valid JSON Schema")
    public void andResponseShouldContainExpectedJsonSchema(String jsonContent) {

        response.then().assertThat().body(matchesJsonSchema(jsonContent).using(jsonSchemaFactory));
    }

    @Step("And response should contain the expected attributes")
    public void andResponseShouldContainExpectedAttributes(JSONObject jsonObject) {

        Map responseMap = response.jsonPath().getMap("");

        assertEquals(jsonObject.toMap().keySet(), responseMap.keySet());

        JSONArray jsonArray = jsonObject.getJSONArray("content");
        List<HashMap> responseList = response.jsonPath().getJsonObject("content");

        if (responseList.isEmpty()) responseList.add(response.jsonPath().getJsonObject(""));

        assertEquals(jsonArray.getJSONObject(0).keySet(), responseList.get(0).keySet());
    }

    @Step("And response should contain the expected attributes")
    public void andResponseShouldContainExpectedAttributes(JSONArray jsonArray) {

        List<HashMap> responseList = response.jsonPath().getJsonObject("");
        int size = jsonArray.length();

        for (int i = 0; i < size; i++) {
            assertEquals(jsonArray.getJSONObject(i).keySet(), responseList.get(i).keySet());
        }
    }
}
