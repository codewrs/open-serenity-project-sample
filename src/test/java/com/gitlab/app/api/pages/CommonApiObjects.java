/*
 *  Copyright (c) 2019.  gitlab.com
 */

package com.gitlab.app.api.pages;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import net.serenitybdd.rest.SerenityRest;

/**
 * Open Serenity Automation Project
 *
 * <p>Common API Objects class
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/12/2019
 */
public class CommonApiObjects {

    private Response response;

    public CommonApiObjects makeGetAPICall(String uri) {

        this.response = SerenityRest.rest().given().contentType(ContentType.JSON).when().get(uri);

        return this;
    }

    public CommonApiObjects makePostAPICall(String uri, String object) {

        this.response =
                SerenityRest.rest()
                        .given()
                        .contentType(ContentType.JSON)
                        .body(object)
                        .when()
                        .post(uri);

        return this;
    }

    public CommonApiObjects makePostAPICall(String uri, Map<String, String> paramMap) {

        RequestSpecification requestSpecification =
                SerenityRest.rest().given().contentType(ContentType.JSON);
        this.response = addQueryParams(requestSpecification, paramMap).when().post(uri);

        return this;
    }

    public CommonApiObjects makePostAPICall(
            String uri, Map<String, String> paramMap, String controlName, File file)
            throws FileNotFoundException {

        if (!file.isFile()) throw new FileNotFoundException("File Not Found [" + file + "]");
        RequestSpecification requestSpecification = SerenityRest.rest().given();

        this.response =
                requestSpecification
                        .spec(addParametersWithFile(paramMap, controlName, file))
                        .when()
                        .post(uri);

        return this;
    }

    public CommonApiObjects makePostAPICallWithBodyContent(String uri, String bodyParam) {

        RequestSpecification requestSpecification =
                SerenityRest.rest().given().contentType(ContentType.JSON);
        this.response = requestSpecification.body(bodyParam).when().post(uri);

        return this;
    }

    public Response getResponse(int statusCode) {
        return response.then().statusCode(statusCode).log().all().extract().response();
    }

    private RequestSpecification addQueryParams(
            RequestSpecification requestSpecification, Map<String, String> map) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            requestSpecification.queryParam(entry.getKey(), entry.getValue());
        }

        return requestSpecification;
    }

    private RequestSpecification addParametersWithFile(
            Map<String, String> map, String controlName, File file) {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.addMultiPart(controlName, file, "application/vnd.ms-excel");
        builder.addQueryParams(map);

        return builder.build();
    }
}
