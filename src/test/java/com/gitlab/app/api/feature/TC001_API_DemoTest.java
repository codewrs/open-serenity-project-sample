/*
 * *
 *    Copyright (c) 2019.  gitlab.com
 *
 */

package com.gitlab.app.api.feature;

import com.gitlab.app.api.steps.CommonAPISteps;
import com.gitlab.app.config.DataProvider;
import com.gitlab.app.testbase.BaseTest;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;

/**
 * Open Serenity Automation Project
 *
 * @author <A HREF="mailto:codewrs@outlook.com">Robin Singh</A>
 * @version 1.0 7/1/2019 1:10 PM
 */
// Define Test data File located at /test/resources/csv
@UseTestDataFrom(value = DataProvider.CSV_TEST_DATA_FILE_PATH + "TC001_API_DemoTest.csv")
@WithTags({
    @WithTag(type = "team", name = "team1"),
    @WithTag(type = "release", name = "sprint-4"),
    @WithTag(type = "epic", name = "API"),
    @WithTag(type = "developer", name = "devwrs"),
    @WithTag(type = "story", name = "AB-1000"),
    @WithTag(type = "suite", name = "regression"),
    @WithTag(type = "suite", name = "sanity"),
    @WithTag(name = "[SC-1000] Some demo API story")
})
public class TC001_API_DemoTest extends BaseTest {

    private String uri, statusCode, inputJsonSchema;

    @Steps private CommonAPISteps commonAPISteps;

    @Title("TC001 Validate REST APIs which provide demo data (#AB-1111)")
    @Test
    public void TC001_Validate_RestAPI_Demo() {
        commonAPISteps.givenThatWeHaveAURI(uri);
        commonAPISteps.whenAGETApiCallIsMade();
        commonAPISteps.thenItShouldReturnAResponseWithStatusCode(statusCode);
        // commonAPISteps.andResponseShouldContainExpectedJsonSchema();
    }
}
