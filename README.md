## OPEN Serenity Automation Project ##


#### Prerequisites: ####
Java 1.8, maven 3.3+ or Gradle 4.0+

#### To execute the project: ####
Open command prompt

Gradle ` gradle clean test` #Default test environment is "qa"
Gradle wrapper ` ./gradlew clean test`
For specific environment use ` test.env ` property variable in command line
` gradle clean test aggregate -Dtest.env=dev`

To zip the serenity report the the gradle task "zipit"

To execute specific package tests ` gradle test --tests com.gitlab.app.tests.* `
To execute specific test class tests 
` gradle clean test aggregate -Dtest.single=TC001_DemoTestt `

- Individual test can be run as Junit test from IDE or by passing class path in CLI.

#### Test reports ####
Go to project source folder
- Individual test case report found at: ` target/site/serenity/ ` and open recent html file
- Aggregate results: Detailed and fluid test results are found at: ` target/site/serenity/index.html `
- Excel format results: ` target/site/serenity/results.csv `
- XML format results for CI parsing: ` target/surefire/*.*xml `

If executed with gradle task "zipit", serenity report files will be zipped and place under the directory "reports"

### Other References: ###
* Serenity BDD Official Documentation: <https://serenity-bdd.github.io/theserenitybook/latest/index.html>
* JavaDocs- <http://javadox.com/net.serenity-bdd/core/1.0.47/overview-summary.html>
* AWS SDK for Java 1.11 (yet to implement with automation framework) <https://aws.amazon.com/sdk-for-java/>


### Libraries Documentation ###
- UI Testing � [Selenium 3.0+](https://seleniumhq.github.io/selenium/docs/api/java/)
- API Testing � [Rest Assured](https://github.com/rest-assured/rest-assured/wiki/Usage) with [JSONPATH](http://jsonpath.com/?) or with [JACKSON/GSON library](https://www.baeldung.com/jackson)
  API Testing Best Practices - <https://blog.philipphauer.de/testing-restful-services-java-best-practices/>
- AWS � [AWS SDK for Java 1.11](https://aws.amazon.com/sdk-for-java/) (yet to implement with automation framework)


### Best Practices: ###

For Better test management we can create profile for regression, smoke or release specific tests 

Maven Eg:
- REGRESSION SUITE: "mvn clean verify -P regression -Dwebdriver.diver=firefox"
- SMOKE TEST SUITE: "mvn clean verify -P smoketests"


### More info ###

Gradle test control from CLI : <https://blog.jdriven.com/2017/10/run-one-or-exclude-one-test-with-gradle/>
