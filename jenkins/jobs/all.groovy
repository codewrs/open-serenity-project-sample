pipelineJob('open-qe-automation') {
    description("OPEN Serenity Automation Project - master branch ci/cd")

    parameters {
        stringParam('BUILD_BRANCH', 'origin/master', 'Target branch for Jenkins build.')
        stringParam('BUILD_NUMBER', '0.0.0.0', 'Build Number')
        stringParam('TARGET_TEST_ENV', 'qa', 'Test environment to run the test on.')
        stringParam('TEST_SUITE', 'suite:sanity', 'Test suite to run.')
    }

    triggers {
        gitlabPush {
            buildOnPushEvents true
            includeBranches 'master'
            buildOnMergeRequestEvents false
            enableCiSkip false
            skipWorkInProgressMergeRequest true
            rebuildOpenMergeRequest 'never'
            setBuildDescription true
        }
    }

    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url repositoryUrl()
                        credentials gitCredentialsId()
                    }
                    branch('*/*')
                }

                scriptPath('jenkins/pipelines/ci-cd.groovy')
            }
        }
    }
}

static def gitCredentialsId() {
    return '84acfa08-b9d8-4b25-a734-85a45440746a'
}

static def repositoryUrl() {
    return 'git@gitlab.com:someteam/projectname.git'
}