pipeline {
    agent any

    options {
        gitLabConnection('Gitlab Connector')
    }

    environment {
        SLACK_URL = 'https://hooks.slack.com/services/abcd'
        REPORT_SLACK_URL = 'https://hooks.slack.com/services/abcd'
        SLACK_MR_CHANNEL = 'merge-request'
        SLACK_JENKINS_BUILD_CHANNEL = 'jenkins-build'
        AWS_REGION = 'us-east-1'
        AWS_ROLE = 'arn:aws:iam::100:role/open-JenkinsBuildStack'
        GITLAB_BUILD_LABEL = 'Jenkins Build'
        BUILD_NUMBER = "${params.BUILD_NUMBER}"
    }

    stages {
        stage('Environment Setup and Checkout') {
            steps {
                setCurrentBuildDisplayName(env.BUILD_NUMBER)

                deleteDir()

                checkout scm

                updateGitlabCommitStatus name: env.GITLAB_BUILD_LABEL, state: 'running'
            }
        }

        stage('Execute Test') {
            steps {
                withAWS(role: env.AWS_ROLE, region: env.AWS_REGION) {
                    sh "chmod +x gradlew"
                    sh "./gradlew clean test -Dtest.env=${params.TARGET_TEST_ENV} -Dtags=${params.TEST_SUITE}"
                }
            }

            post {
                always {
                    publishHTML(target: [
                            allowMissing         : true,
                            alwaysLinkToLastBuild: false,
                            keepAll              : true,
                            reportDir            : 'build/reports/features/test',
                            reportFiles          : 'index.html',
                            reportName           : "Tests Report"
                    ])

                    junit('build/test-results/test/*.xml')
                }
            }
        }

        stage("Deliver Test Report") {
            environment {
                DATE_NOW = new Date().format('yy-MM-dd.HH-mm-ss', TimeZone.getTimeZone('UTC'))
                TEST_SUITE_NAME = params.TEST_SUITE.replaceAll(":", "_")
                TEST_REPORT_FILE_NAME = "${env.BUILD_NUMBER}_${env.TEST_SUITE_NAME}_${params.TARGET_TEST_ENV}_${env.DATE_NOW}.zip"
            }
            steps {
                sh """
                    zip -r ${env.TEST_REPORT_FILE_NAME} target/site/*
                    mv ${env.TEST_REPORT_FILE_NAME} target
                """

                uploadAndPreSign(env.TEST_REPORT_FILE_NAME)
            }
        }
    }

    post {
        always {
            slackNotifier()

            deleteDir()
        }

        failure {
            updateGitlabCommitStatus name: env.GITLAB_BUILD_LABEL, state: 'failed'
        }

        success {
            updateGitlabCommitStatus name: env.GITLAB_BUILD_LABEL, state: 'success'
        }
    }
}

// Generate Pre signed URL for s3 objects
void uploadAndPreSign(filename) {
    def s3Bucket = 'scpe-chc-qa-print'
    def s3FilePath = "p2/qa/reports/"
    def reportsFilePath = "target/${filename}"

    s3Upload(file: reportsFilePath, bucket: s3Bucket, path: s3FilePath)

    def preSignedURL = s3PresignURL(bucket: s3Bucket, key: "${s3FilePath}${filename}", durationInSeconds: 604800)

    slackNotify(slackPayloadCreator(preSignedURL))
}

void slackNotify(payload) {
    echo 'Slack Notify'

    sh """
        curl -X POST \
        ${env.REPORT_SLACK_URL} \
        -H 'Content-Type: application/x-www-form-urlencoded' \
        -d 'payload=${payload}'
    """
}

String slackPayloadCreator(url) {
    def trigger = currentBuild.getBuildCauses().shortDescription.toString().replace("\"", "\\\"")
    def color = "good"
    def result = "Pass"

    if (currentBuild.currentResult != 'SUCCESS') {
        color = "danger"
        result = "Fail"
    }

    def payload = """
        {
            "channel": "#p2-test-report",
            "username": "Jenkins Server",
            "attachments": [
                {
                    "fallback": "${params.TEST_SUITE} Test on ${params.TARGET_TEST_ENV} - ${result}",
                    "color": "${color}",
                    "pretext": "${params.TEST_SUITE} Test on ${params.TARGET_TEST_ENV} - ${result}",
                    "title": "OPEN Serenity Automation Test Report",
                    "title_link": "${url}",
                    "text": "${env.JOB_NAME} #${env.BUILD_NUMBER} \n${trigger}",
                    "fields": [
                        {
                            "title": "Test Result",
                            "value": "${result}"
                        }
                    ]
                }
            ]
        }
    """
    return URLEncoder.encode(payload, "UTF-8")
}

void setCurrentBuildDisplayName(displayName) {
    currentBuild.displayName = displayName
}